Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root "home#index"

  resources :most_relevants, defaults: { format: :json }, only: [:index]

  resources :most_mentions, defaults: { format: :json }, only: [:index]
end
