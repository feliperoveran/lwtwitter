class Tweet
  include ActiveModel::Serializers::JSON

  attr_accessor :screen_name, :profile_url, :followers_count, :retweet_count, :likes, :text, :created_at, :url

  def self.initialize_from_json(json)
    instance = new

    instance.from_json(json)
  end

  def attributes=(hash)
    hash = hash.with_indifferent_access

    self.screen_name = hash[:user][:screen_name]

    self.profile_url = "https://twitter.com/#{screen_name}"

    self.followers_count = hash[:user][:followers_count]

    self.retweet_count = hash[:retweet_count]

    self.likes = hash[:user][:favourites_count]

    self.text = hash[:text]

    self.created_at = hash[:created_at].to_datetime

    self.url = "https://twitter.com/statuses/#{hash[:id_str]}"
  end

  def attributes
    instance_values
  end

  def self.get_tweets(username)
    response = RestClient.get("http://tweeps.locaweb.com.br/tweeps", username: username)

    statuses = JSON.parse(response.body)

    statuses.with_indifferent_access[:statuses]
  end

  def self.with_locaweb_mentions(tweets)
    tweets.select do |tweet|
      tweet[:entities][:user_mentions].any? { |um| um[:id] == 42 }
    end
  end

  def self.not_locaweb_reply(tweets)
    tweets.reject { |tweet| tweet[:in_reply_to_user_id] == 42 }
  end

  def self.relevant_tweets(tweets)
    with_locaweb_mentions(tweets) & not_locaweb_reply(tweets)
  end
end
