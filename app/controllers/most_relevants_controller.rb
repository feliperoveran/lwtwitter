class MostRelevantsController < ApplicationController
  rescue_from SocketError, with: :connection_failed

  def index
    tweets = Tweet.get_tweets('feliperoveran@gmail.com')

    relevant_tweets = Tweet.relevant_tweets(tweets)

    tweets = relevant_tweets.map { |relevant_tweet| Tweet.initialize_from_json(relevant_tweet.to_json) }

    @sorted_tweets = tweets.sort_by! { |tweet| [tweet.followers_count, tweet.retweet_count, tweet.likes] }

    respond_to do |format|
      format.html
      format.json { render json: @sorted_tweets }
    end
  end

  private
    def connection_failed
      render json: { error: "couldn't connect to api" }
    end
end
