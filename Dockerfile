# using the official 'rails:onbuild' that is available on the Docker Hub
# this provides nice defaults
# 1. System dependencies and recommended packages are already preinstalled.
# 2. rails server is launched when container is started.
# 3. Updates to the Gemfile and Gemfile.lock are recognized during builds, and the appropriate bundle install step is triggered when needed.
# 4. Application code is installed into /usr/src/app on a virtual filesystem. If the Gemfile hasn’t been changed since the previous build, the previous, cached bundle install step is used.

# to create a Docker image just run: docker build -t lwtwitter .

# then, to run the server: docker run -p 3000:3000 lwtwitter

FROM rails:onbuild
