# README


## Using Ruby on Rails 5.0.1 and Ruby 2.3.0

## Install
`
bundle install
`

## Running the tests
### Using Rspec 3.5 with VCR and Webmock gems

`
bundle exec rspec
`

## Running the Server
`
rails s
`

## Using Bootstrap v4.0.0 alpha3

## Docker
There is a Dockerfile on the root directory.
To create a Docker image, run `docker build -t lwtwitter .`

Then, run `docker run -p 3000:3000 lwtwitter` to run the server from the Docker image

The image is created using the official rails:onbuild that is available on the Docker Hub.

Explanations can be found on the Dockerfile itself.

## Resources
There are two available resources, /most_relevants and /most_mentions, both rendering JSON.

In order to view a user friendly response in HTML, simply append .html to the desired resource. 

Visiting /most_relevants.html or most_mentions.html will render a HTML page with the results.

## Online availability
The app is available online at http://feliperoveranlwtwitter.herokuapp.com/ with the following resources

http://feliperoveranlwtwitter.herokuapp.com/most_mentions

http://feliperoveranlwtwitter.herokuapp.com/most_mentions.html

http://feliperoveranlwtwitter.herokuapp.com/most_relevants

http://feliperoveranlwtwitter.herokuapp.com/most_relevants.html