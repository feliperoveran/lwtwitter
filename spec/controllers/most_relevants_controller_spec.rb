require 'rails_helper'

RSpec.describe MostRelevantsController, "#index" do
  it "it gets the tweets from the api" do
    relevant_tweets = [double, double]
    allow(Tweet).to receive(:relevant_tweets).and_return(relevant_tweets)

    initialized_tweet = double
    allow(Tweet).to receive(:initialize_from_json).and_return(initialized_tweet)

    allow(initialized_tweet).to receive(:sort_by!)
    allow(initialized_tweet).to receive(:followers_count)
    allow(initialized_tweet).to receive(:retweet_count)
    allow(initialized_tweet).to receive(:likes)

    expect(Tweet).to receive(:get_tweets)

    get :index
  end

  it "filters the relevant tweets" do
    tweets = [double, double]
    allow(Tweet).to receive(:get_tweets).and_return(tweets)

    initialized_tweet = double
    allow(Tweet).to receive(:initialize_from_json).and_return(initialized_tweet)

    allow(initialized_tweet).to receive(:sort_by!)
    allow(initialized_tweet).to receive(:followers_count)
    allow(initialized_tweet).to receive(:retweet_count)
    allow(initialized_tweet).to receive(:likes)

    expect(Tweet).to receive(:relevant_tweets).with(tweets).and_return([double, double])

    get :index
  end

  it "initializes tweet objects with json" do
    tweets = [double, double]
    allow(Tweet).to receive(:get_tweets).and_return(tweets)

    relevant_tweets = [double, double]
    allow(Tweet).to receive(:relevant_tweets).and_return(relevant_tweets)

    initialized_tweet = double
    allow(Tweet).to receive(:initialize_from_json).and_return(initialized_tweet)

    allow(initialized_tweet).to receive(:sort_by!)
    allow(initialized_tweet).to receive(:followers_count)
    allow(initialized_tweet).to receive(:retweet_count)
    allow(initialized_tweet).to receive(:likes)

    expect(Tweet).to receive(:initialize_from_json)

    get :index
  end

  it "sorts the tweets by the correct arguments" do
    tweets = [double, double]
    allow(Tweet).to receive(:get_tweets).and_return(tweets)

    relevant_tweets = [double, double]
    allow(Tweet).to receive(:relevant_tweets).and_return(relevant_tweets)

    initialized_tweet = double
    allow(Tweet).to receive(:initialize_from_json).and_return(initialized_tweet)

    allow(relevant_tweets).to receive(:map).and_return(tweets)

    sorted_tweet = double
    expect(tweets).to receive(:sort_by!).and_yield(sorted_tweet)
    expect(sorted_tweet).to receive(:followers_count)
    expect(sorted_tweet).to receive(:retweet_count)
    expect(sorted_tweet).to receive(:likes)

    get :index
  end

  it "renders the tweets as json" do
    tweets = [double, double]
    allow(Tweet).to receive(:get_tweets).and_return(tweets)

    relevant_tweets = [double, double]
    allow(Tweet).to receive(:relevant_tweets).and_return(relevant_tweets)

    initialized_tweet = double
    allow(Tweet).to receive(:initialize_from_json).and_return(initialized_tweet)

    allow(relevant_tweets).to receive(:map).and_return(tweets)

    sorted_tweets = [double, double]
    allow(tweets).to receive(:sort_by!).and_return(sorted_tweets)
    allow(initialized_tweet).to receive(:followers_count)
    allow(initialized_tweet).to receive(:retweet_count)
    allow(initialized_tweet).to receive(:likes)

    get :index, format: :json

    expect(response.body).to eq sorted_tweets.to_json
  end
end
