require 'rails_helper'

RSpec.describe Tweet do
  let(:tweets) do
    VCR.use_cassette 'model/tweets' do
      response = RestClient.get("http://tweeps.locaweb.com.br/tweeps", username: "feliperoveran@gmail.com")

      JSON.parse(response.body).with_indifferent_access[:statuses]
    end
  end

  let(:with_locaweb_mentions) do
    tweets.select do |tweet|
      tweet[:entities][:user_mentions].any? { |um| um[:id] == 42 }
    end
  end

  let(:not_locaweb_reply) do
    tweets.reject { |tweet| tweet[:in_reply_to_user_id] == 42 }
  end

  describe ".initialize_from_json(json)" do
    it "initializes an instance with the correct screen_name attribute" do
      json = tweets.first

      tweet = described_class.initialize_from_json(json.to_json)

      expect(tweet.screen_name).to eq json[:user][:screen_name]
    end

    it "initializes an instance with the correct profile_url attribute" do
      json = tweets.first

      tweet = described_class.initialize_from_json(json.to_json)

      expect(tweet.profile_url).to eq "https://twitter.com/#{tweet.screen_name}"
    end


    it "initializes an instance with the correct followers_count attribute" do
      json = tweets.first

      tweet = described_class.initialize_from_json(json.to_json)

      expect(tweet.followers_count).to eq json[:user][:followers_count]
    end

    it "initializes an instance with the correct retweet_count attribute" do
      json = tweets.first

      tweet = described_class.initialize_from_json(json.to_json)

      expect(tweet.retweet_count).to eq json[:retweet_count]
    end

    it "initializes an instance with the correct likes attribute" do
      json = tweets.first

      tweet = described_class.initialize_from_json(json.to_json)

      expect(tweet.likes).to eq json[:user][:favourites_count]
    end

    it "initializes an instance with the correct text attribute" do
      json = tweets.first

      tweet = described_class.initialize_from_json(json.to_json)

      expect(tweet.text).to eq json[:text]
    end

    it "initializes an instance with the correct created_at attribute" do
      json = tweets.first

      tweet = described_class.initialize_from_json(json.to_json)

      expect(tweet.created_at).to eq json[:created_at].to_datetime
    end

     it "initializes an instance with the correct url attribute" do
      json = tweets.first

      tweet = described_class.initialize_from_json(json.to_json)

      expect(tweet.url).to eq "https://twitter.com/statuses/#{json[:id_str]}"
    end
  end

  describe "#attributes" do
    it "returns the instance values" do
      tweet = described_class.new

      expect(tweet).to receive(:instance_values)

      tweet.attributes
    end
  end

  describe ".get_tweets(username)" do
    it "calls the api with the correct url and username" do
      url = "http://tweeps.locaweb.com.br/tweeps"
      username = "user@example.com"

      response = double.as_null_object

      allow(JSON).to receive(:parse).and_return(double.as_null_object)

      expect(RestClient).to receive(:get).with(url, username: username).and_return(response)

      described_class.get_tweets(username)
    end

    it "parses response body as json" do
      username = "user@example.com"

      response = double

      allow(RestClient).to receive(:get).and_return(response)

      expect(response).to receive(:body)
      expect(JSON).to receive(:parse).and_return(double.as_null_object)

      described_class.get_tweets(username)
    end

    it "returns the :status part of the parsed json" do
      username = "user@example.com"

      response = double.as_null_object
      statuses = double

      allow(RestClient).to receive(:get).and_return(response)
      allow(JSON).to receive(:parse).and_return(statuses)

      expect(statuses).to receive_message_chain(:with_indifferent_access, :[]).with(no_args).with(:statuses)

      described_class.get_tweets(username)
    end
  end

  describe ".not_locaweb_reply(tweets)" do
    it "returns tweets that are not a reply to @locaweb" do
      result = described_class.not_locaweb_reply(tweets)

      expect(result).to eq not_locaweb_reply
    end
  end

  describe ".with_locaweb_mentions(tweets)" do
    it "returns tweets that mention @locaweb" do
      result = described_class.with_locaweb_mentions(tweets)

      expect(result).to eq with_locaweb_mentions
    end
  end

  describe ".relevant_tweets(tweets)" do
    it "returns tweets that are not a reply to @locaweb and that mention @locaweb" do
      result = described_class.relevant_tweets(tweets)

      expect(result).to eq (with_locaweb_mentions & not_locaweb_reply)
    end
  end
end
